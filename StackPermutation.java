/*
 * @author Daniel Jackson
 * 
 */

import java.util.*;
/*
 * creates new state objects to keep track of the permutation process. 
 * *note: is not resetting correctly after an initial run.
 */
public class StackPermutation {
	static State S1;	//initialized to 0, stores the permutation after rule is applied
	static State S2;	//initialized to 0, stores S1 after permutation
	static State S0;	//Stores the initial number
	static State SN;	//stores the goal state for the permutation
	static State SG; 	//copy of SN, only if SN is a permutation of the number
	static Stack<State> Q;
	static boolean cont = true;
	private static Scanner scan;

	public static void Permute(int k, int p) {
		cont = true;
		Q = new Stack<State>();
		State S1 = new State();
		State S2 = new State();
		State S0 = new State();
		State SN = new State();
		State SG = new State();
			
		SN.pushtoP(p);		
		S0.setInit(k);
		Q.push(S0);
		/*
		 * while the Queue is not empty and cont = true, create new temp
		 * fill temp with Q, copy temp to S1
		 */
		while (Q.empty() != true && cont == true) {
			State tmp = new State();
			tmp = Q.pop();
			S1.copy(tmp);
			

			if (S1.R1test() == true) {
				S2 = new State();
				S2.copy(S1);				
				S2.R1();
				
				if (S2.isEquals(SN)) {
					SG.copy(S2);
					cont = false;
				} else {
					Q.push(S2);
				}

			}			
			if (S1.R2test() == true) {
				S2 = new State();
				S2.copy(S1);
				S2.R2();				
				if (S2.isEquals(SN)) {
					SG.copy(S2);
					cont = false;
				}

				else {
					Q.push(S2);
				}
			}			
			if (S1.R3test() == true) {
				S2 = new State();
				S2.copy(S1);
				S2.R3();				
				if (S2.isEquals(SN)) {
					SG.copy(S2);
					cont = false;
				} else {
					Q.push(S2);
				}
			}			
		}
		System.out.println("Permutation: " + S0.Init);
		System.out.println("Goal " + SG.Perm);
		if (SG.testEmpt() == true) {
			System.out.println("Not a stack permutation");
		} else {
			System.out.println("Stack Permutation. The list of rules are: "
					+ SG.Rule);
		}
	}
	
	public static void clearAll(){
		S0.clearAll();
		S1.clearAll();
		S2.clearAll();
		SN.clearAll();
		SG.clearAll();
		
	}
	/**
	 * @param args  the command line arguments
	 */

	public static void main(String[] args) {
	
		int finish = 1;
		int s;
		int j;
		

		while (finish != 0) {	
			scan = new Scanner(System.in);
			
			System.out.println("This program attempts to find a permutation of a string of numbers.\n" + "Please enter 4 or more numbers.");
			System.out.println("Please enter a number to calculate: ");
			s = scan.nextInt();
			System.out.println("Please enter a desired goal state: ");
			j = scan.nextInt();
			Permute(s, j);
			System.out.println("Continue? y(1) n(0)");
			finish = scan.nextInt();
		}

	}
}
