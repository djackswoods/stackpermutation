/*
 * @Author: Daniel Jackson
 */
import java.util.*;

public class State {
	ArrayList<Object> Init = new ArrayList<>();
	ArrayList<Object> Rule = new ArrayList<>();
	ArrayList<Object> Perm = new ArrayList<>();
	Stack<Integer> stack = new Stack<Integer>();

	public void setInit(int k) {
		while (k > 0) {
			Init.add(k % 10);
			k /= 10;
		}
		Collections.reverse(Init);
	}

	@SuppressWarnings("unchecked")
	public void copy(State s) {
		Perm = (ArrayList<Object>) s.Perm.clone();
		Init = (ArrayList<Object>) s.Init.clone();
		Rule = (ArrayList<Object>) s.Rule.clone();
		stack = (Stack<Integer>) s.stack.clone();
	}
	
	public boolean Empt() {
		return Init.isEmpty();
	}
	
	public boolean isEquals(State k) {
		if (this.Perm.equals(k.Perm) && this.stack.equals(k.stack) && this.Init.equals(k.Init)) {
			return true;
		} else {
			return false;
		}
	}
	
	public void clearAll(){
		if(!Init.isEmpty() && !Rule.isEmpty() && !stack.isEmpty() && !Perm.isEmpty()){
		Init.clear();
		Rule.clear();
		stack.clear();
		Perm.clear();
		}else{
			return;
		}
	}

	public void pushtoP(int a) {
		while (a > 0) {
			Perm.add(a % 10);
			a /= 10;
		}
		Collections.reverse(Perm);
	}

	public ArrayList<Object> getP() {
		return Perm;
	}

	public void R1() {
		if (Init.size() != 0) {
			int tmp = (int) Init.get(0);
			Perm.add(0, tmp);
			Init.remove(0);
			Rule.add(1);
		}
	}

	public boolean R1test() {
		if (Init.size() != 0) {
			return true;
		} else {
			return false;
		}
	}

	public void R2() {
		if (Init.size() != 0) {
			int tmp = (int) Init.get(0);
			Init.remove(0);
			stack.push(tmp);
			Rule.add(2);
		}
	}

	public boolean R2test() {
		if (Init.size() != 0) {
			return true;
		} else {
			return false;
		}

	}

	public void R3() {
		if (stack.empty() == false) {
			Perm.add(stack.pop());
			Rule.add(3);

		}
	}

	public boolean R3test() {
		if (stack.empty() == false) {
			return true;
		} else {
			return false;
		}
	}	

	public boolean testEmpt() {
		if (Init.isEmpty() == true && Perm.isEmpty() == true && stack.isEmpty() == true) {
			return true;
		} else {
			return false;
		}
	}



}
